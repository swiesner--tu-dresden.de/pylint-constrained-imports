"""Checkers that can be registered as plugins"""
from typing import Optional, List, Tuple
from astroid import nodes
from pylint.checkers import BaseChecker
from pylint.interfaces import IAstroidChecker
from pylint.lint import PyLinter


class ConstrainedImportsChecker(BaseChecker):
    """Check for constrained imports.

    The checker visits both pure `import` and `from foo import bar` nodes
    and checks for configured constraints.
    """

    __implements__ = IAstroidChecker

    name = "constrained_imports"
    priority = -1
    msgs = {
        "W6500": (
            "Constrained import used.",
            "used-constrained-import",
            "Constrained imports should not be used inside of this project.",
        )
    }
    options = (
        (
            "constrained-imports",
            {
                "type": "csv",
                "metavar": "<path to module>",
                "help": "List of modules to be constrained for import,"
                "separated by comma.",
            },
        ),
        (
            "constrained-imports-from",
            {
                "type": "csv",
                "metavar": "<path to module>:<name1>+<name2>",
                "help": "List of modules followed by name to be constrained "
                "for import, separated by comma.",
            },
        ),
    )

    def __init__(self, linter: PyLinter = None) -> None:
        super().__init__(linter)
        self.constrained_imports_from = {}
        self.constrained_imports = []
        self._configured = False

    def _parse_config(self) -> None:
        """Parse configuration.

        The CSV strings from internal configuration will be parsed and split
        into lists kept inside of the checker.

        Returns:
            None
        """
        if self._configured:
            return
        constrained_imports_from: List[str] = self.option_value(
            "constrained-imports-from"
        )
        constrained_imports: List[str] = self.option_value(
            "constrained-imports"
        )
        if constrained_imports_from is not None:
            for constrained_import in constrained_imports_from:
                _tmp = constrained_import.split(":")
                self.constrained_imports_from[_tmp[0]] = _tmp[1].split("+")
        if constrained_imports is not None:
            self.constrained_imports = constrained_imports
        self._configured = True

    def visit_importfrom(self, node: nodes.ImportFrom) -> None:
        """Visit `from foo import bar` nodes.

        If the visited node is constrained per configuration, the linter will
        emit a warning.

        Args:
            node: the actual node processed

        Returns:
            None
        """
        self._parse_config()
        if node.modname in self.constrained_imports_from:
            imported_name: Tuple[str, Optional[str]]
            for imported_name in node.names:
                name, _ = imported_name
                if name in self.constrained_imports_from[node.modname]:
                    self.add_message("used-constrained-import", node=node)

    def visit_import(self, node: nodes.Import) -> None:
        """Visit `import foo` nodes.

        If the visited node is constrained per configuration, the linter will
        emit a warning.

        Args:
            node: the actual node processed

        Returns:
            None
        """
        self._parse_config()
        imported_name: Tuple[str, Optional[str]]
        for imported_name in node.names:
            name, _ = imported_name
            if name in self.constrained_imports:
                self.add_message("used-constrained-import", node=node)
