"""Initialisation of needed components"""
from pylint.lint import PyLinter
from .checkers.constrained_imports import ConstrainedImportsChecker


def register(linter: PyLinter) -> None:
    """Register included checker(s).

    Args:
        linter: the actual linter instance running

    Returns:
        None
    """
    linter.register_checker(ConstrainedImportsChecker(linter))
