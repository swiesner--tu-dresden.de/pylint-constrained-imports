#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import path
from setuptools import setup, find_packages


here = path.abspath(path.dirname(__file__))
with open(path.join(here, 'README.md')) as fin:
    long_description = fin.read()


setup(
    name='pylint-constrained-imports',
    version='0.0.1',
    author='Sebastian Wiesner',
    author_email='Sebastian.Wiesner@tu-dresden.de',
    maintainer='Sebastian Wiesner',
    maintainer_email='Sebastian.Wiesner@tu-dresden.de',
    license='MIT',
    url='https://gitlab.hrz.tu-chemnitz.de/swiesner--tu-dresden.de/'
        'pylint_constrained_imports',
    description='A pylint plugin to restrict imports.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    packages=find_packages(exclude=['tests']),
    install_requires=[
        'pylint',
        'pytest',
    ],
    python_requires='>=3.5',
    classifiers=[
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Quality Assurance',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: Implementation :: CPython',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: MIT License',
    ],
    tests_require=['pytest', 'pylint'],
    keywords=['pylint', 'plugin'],
)