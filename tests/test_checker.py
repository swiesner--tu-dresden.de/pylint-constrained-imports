"""Tests for ConstrainedImportsChecker"""
from astroid import extract_node
from pylint.testutils import CheckerTestCase, MessageTest
from pylint_constrained_imports.checkers.constrained_imports import (
    ConstrainedImportsChecker,
)


class TestConstrainedImportsChecker(CheckerTestCase):
    """Test class for pytest"""

    CHECKER_CLASS = ConstrainedImportsChecker

    def test_finds_constrained_import(self) -> None:
        """Test finds constrained import as per configuration"""
        self.checker.config.constrained_imports = ["foo"]
        import_node = extract_node("import foo")
        with self.assertAddsMessages(
            MessageTest(msg_id="used-constrained-import", node=import_node)
        ):
            self.checker.visit_import(import_node)

    def test_finds_no_constrained_import(self) -> None:
        """Test doesn't find constrained import as per configuration"""
        self.checker.config.constrained_imports = ["foo"]
        import_node = extract_node("import bar")
        with self.assertNoMessages():
            self.checker.visit_import(import_node)

    def test_finds_constrained_import_from(self) -> None:
        """Test finds constrained import from as per configuration"""
        self.checker.config.constrained_imports_from = ["foo:bar"]
        import_node = extract_node("from foo import bar")
        with self.assertAddsMessages(
            MessageTest(msg_id="used-constrained-import", node=import_node)
        ):
            self.checker.visit_importfrom(import_node)

    def test_finds_multiple_constrained_imports_from(self) -> None:
        """Test finds multiple constrained import from as per configuration"""
        self.checker.config.constrained_imports_from = ["foo:bar+baz"]
        import_node = extract_node("from foo import bar, baz")
        with self.assertAddsMessages(
            MessageTest(msg_id="used-constrained-import", node=import_node),
            MessageTest(msg_id="used-constrained-import", node=import_node),
        ):
            self.checker.visit_importfrom(import_node)

    def test_finds_no_constrained_import_from(self) -> None:
        """Test doesn't find constrained from import as per configuration"""
        self.checker.config.constrained_imports_from = ["foo:bar"]
        import_node = extract_node("from foo import baz")
        with self.assertNoMessages():
            self.checker.visit_import(import_node)
