# pylint-constrained-imports

A PyLint plugin that checks for constrained imports.

## Installation

Requirements:

- `pylint`

To install:

````bash
$ pip install git+https://gitlab.hrz.tu-chemnitz.de:swiesner--tu-dresden.de/pylint-constrained-imports.git
````

## Usage

Enable via command line option `--load-plugins`

```bash
$ pylint --load-plugins pylint_constrained_imports --constrained-imports <path.to.module>[,<path.to.another.module]
```
or
```bash
$ pylint --load-plugins pylint_constrained_imports --constrained-imports-from <path.to.module1:name1[+another_name]>[,<path.to.module2:name1>]
```
or a combination of both options.

Or in `.pylintrc`
```ini
[MASTER]
load-plugins=pylint_constrained_imports
constrained-imports=<path.to.module>[,<path.to.another.module]
constrained-imports-from=<path.to.module1:name1[+another_name]>[,<path.to.module2:name1>]
```

## Emitted Pylint Warnings

### `used-constrained-import`

Warning when a constrained import or import from is used inside a module, e.g.

```python
# module main.py
import sys
from django.contrib.auth.decorators import login_required
```

```bash
$ pylint --load-plugins pylint_constrained_imports --constrained-imports sys --constrained-imports-from django.contrib.auth.decorators:login_required main.py
```

## License

`pylint-constrained-imports` is available under [MIT license](LICENSE).